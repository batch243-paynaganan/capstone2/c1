// dependencies
const express=require('express')
const mongoose=require('mongoose')
const cors=require('cors')
const userRoutes=require('./routes/userRoutes')
const productRoutes=require('./routes/productRoutes')
const app=express()

// middlewares
app.use(cors())
app.use(express())
app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use('/users',userRoutes)
app.use('/products', productRoutes)

// db connection
mongoose.set('strictQuery',true)
mongoose.connect("mongodb+srv://admin:admin@zuittbatch243.e2dwbki.mongodb.net/capstone2?retryWrites=true&w=majority")
// db terminal prompt
mongoose.connection.on('error',()=>console.error.bind(console,"connection error"))
mongoose.connection.once('open',()=>console.log('Now connected to MongoDB Atlas'))

// listen
app.listen(process.env.PORT || 3000,()=>{console.log(`API is now online on port ${process.env.PORT || 3000}`)})