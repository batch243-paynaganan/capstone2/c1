const express=require('express')
const router=express.Router()
const productControllers=require('../controllers/productControllers')
const auth=require('../auth')

// no params
router.post('/createProduct',auth.verify,productControllers.createProduct) //create product
router.get('/active',productControllers.activeProducts) //active products
router.get('/all',auth.verify,productControllers.allProducts) //all products
router.get('/bestsellers',productControllers.bestSellers) //best selling products
router.get('/sale',productControllers.onSale) //onsale products

// with params
router.put('/update/:productId',auth.verify,productControllers.updateProduct) //update a product
router.patch('/archives/:productId',auth.verify,productControllers.archiveProduct) //archive a product
router.get('/:productId',productControllers.getProduct)
router.patch('/bestsellerUpdate/:productId',auth.verify,productControllers.updateBestSellers) //update a best seller
router.patch('/updateOnSale/:productId',auth.verify,productControllers.updateBestSellers) //update an on sale

module.exports=router;