const express=require('express')
const router=express.Router()
const userControllers=require('../controllers/userControllers')
const auth=require('../auth')

// no params
router.post('/register',userControllers.registration)
router.post('/login',userControllers.login)
router.get('/profile',userControllers.userDetails)

// with params
router.post('/add/:productId',auth.verify,userControllers.cart)
router.patch('/updateRole/:userId',auth.verify,userControllers.updateRole)
router.post('/remove/:productId',auth.verify,userControllers.removeToCart)
router.post('/checkout/:userId',auth.verify,userControllers.transaction)

module.exports=router