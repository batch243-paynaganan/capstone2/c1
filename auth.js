const jwt=require('jsonwebtoken')
const User=require('./models/User')
const secret='ecommerceAPI'

// create a token
module.exports.createToken=(result)=>{
    const data={
        id: result._id,
        email: result.email,
        isAdmin: result.isAdmin
    }
    return jwt.sign(data,secret,{})
}

// verify a token
module.exports.verify=(req,res,next)=>{
    let token=req.headers.authorization;
    console.log(token);
    if(token){
        token=token.slice(7,token.length)
        return jwt.verify(token,secret,err=>{
            if(err){
                return res.send('Invalid token')
            }else{
                next()
            }
        })
    }else{
        return res.send('No token provided')
    }
}

// decode a token
module.exports.decode=(token)=>{
    if(!token){
        return null
    }else{
        token=token.slice(7,token.length)
        return jwt.verify(token,secret,err=>{
            if(err){
                return null
            }else{
                return jwt.decode(token,{complete:true}).payload
            }
        })
    }
}