// dependencies
const User=require('../models/User')
const bcrypt=require('bcrypt')
const auth=require('../auth')
const Products = require('../models/Products')

// registration and availability
module.exports.registration=(req,res)=>{
    const newUser = new User(
        {
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            password: bcrypt.hashSync(req.body.password,10),
            mobileNo: req.body.mobileNo,
            address:{
                houseNo: req.body.houseNo,
                street: req.body.street,
                barangay: req.body.barangay,
                city: req.body.city,
                province: req.body.province
            },
            cartQuantity: req.body.cartQuantity
        })
    return User.find({email:req.body.email})
    .then(result=>{
        if(result.length>0){
            return res.send(`Sorry beh, taken na sya. Move on na.`)
        }else{
            return newUser.save()
            .then(user=>{
                console.log(newUser);
                res.send(`Registered yarn!`)
            })
            .catch(err=>{
                res.send(`Try lang ng try. Fighting girl!`)
            })
        }})
    .catch(err=>{
        res.send(err)
    })
}

// login
module.exports.login=(req,res)=>{
    return User.findOne({email:req.body.email})
    .then(result=>{
        if(!result){
            return res.send(`Register ka muna.`)
        }else{
            const checkPassword=bcrypt.compareSync(req.body.password, result.password);
            if(checkPassword){
                return res.send({accessToken: auth.createToken(result)})
            }else{
                return res.send('Hindi yan ang password mo, alalahanin mong mabuti')
            }}
    })
}

// update a role
module.exports.updateRole=(req,res)=>{
    const userData=auth.decode(req.headers.authorization)
    const idUpdate=req.params.userId;
    if(userData.isAdmin){
        return User.findById(idUpdate)
        .then(result=>{
            const update={isAdmin: !result.isAdmin}
            return User.findByIdAndUpdate(idUpdate,update,{new:true})
            .then(document=>{
                document.password="john cena"
                return res.send(document)
            })
            .catch(err=>{res.send(err)})
        })
        .catch(err=>{res.send(err)})
    }else{
        return res.send('Hindi ka po admin')
    }
}

// retrieve user details
module.exports.userDetails=(req,res)=>{
    const userData=auth.decode(req.headers.authorization)
    return User.findById(userData.id)
    .then(result=>{
        result.password="john cena"
        return res.send(result)
    })
    .catch(err=>res.send(err))
}

// add to cart
module.exports.cart=async(req,res)=>{
    const userData=auth.decode(req.headers.authorization)
    const productId=req.params.productId
    const {quantity}=req.body
    if(!userData.isAdmin){
        const product=await Products.findById(productId)
        let subtotal=quantity*product.productPrice
        const user = await User.findById(userData.id)

        user.cart.push({
                productId: product._id,
                quantity,
                subtotal
         })
        user.save()
        return res.send(user)

        }else{
            res.send('Admin ka beh')
        }
}

// remove to cart
module.exports.removeToCart=async(req,res)=>{
    const userData=auth.decode(req.headers.authorization)
    const productId=req.params.productId
    const {quantity}=req.body
    if(!userData.isAdmin){
        const product=await Products.findById(productId)
        let subtotal=quantity*product.productPrice
        const user = await User.findById(userData.id)

        user.cart.splice({
                productId: product._id,
                quantity,
                subtotal
         })
        user.save()
        return res.send(user)

        }else{
            res.send('Admin ka beh')
        }
}

// checkout
module.exports.transaction=async(req,res)=>{
    const userData=auth.decode(req.headers.authorization)
    const productId=req.params.productId
    const quantity = req.body.quantity
    const totalQty=quantity
    const subtotal=req.body.subtotal
    const totalPrice=subtotal;
    if(!userData.isAdmin){
        const product=await Products.findById(productId)
        // subtracts the stocks
        product.stocks-=quantity
        // adds the items to the order array
        product.order.push({
            userId: userData.id,
            totalQty,
            totalPrice
        })

        const user=await User.findById(userData.id)
        // removes the items from the cart
        user.cart.splice({
            productId: product._id,
            quantity,
            subtotal
        })

        return (user && product) ? res.send('goods') : res.send(false)
        
    }else{
        return res.send('Access denied')
    }
}