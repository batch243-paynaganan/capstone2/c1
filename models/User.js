const mongoose=require('mongoose')

const userSchema=new mongoose.Schema({
    firstName: {type:String,required:[true,'pamystery effect yarn?']},
    lastName: {type:String,required:[true,'wala ka family name beh?']},
    email: {type:String,required:[true,'so pano ka maglolog in?']},
    password: {type:String,required:[true,'open source account yarn?']},
    isAdmin: {type:Boolean,default: false},
    createdOn:{type: Date, default: new Date()},
    mobileNo: {type:String,required:[true, 'di naman kami scammer bhie']},
    address:[{
        houseNo: {type: String},
        street: {type: String, required:[true, 'street name is required']},
        barangay: {type: String, required:[true, 'barangay name is required']},
        city: {type: String, required:[true, 'city name is required']},
        province: {type: String, required:[true, 'province name is required']}
    }],
    cartQuantity:{type:Number,default:0},
    cart:[{
        productId:{type: String,required:true},
        quantity:{type:Number, required:[true, "quantity is required"]},
        subtotal:{type: Number,default: 0},
        addedOn:{type: Date,default:new Date()}
    }]
})

module.exports=mongoose.model('User',userSchema)