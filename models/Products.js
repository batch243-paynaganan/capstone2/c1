const mongoose=require('mongoose')

const productSchema=new mongoose.Schema({
    productName: {type: String, required:[true, "Product is required"]},
    productDescription: {type: String},
    productCollection:{type:String},
    productPrice: {type: Number, required: [true, "Price is required"]},
    productColor: {type: String},
    productSize: {type: String},
    isActive: {type: Boolean, default: true},
    isBestseller: {type: Boolean, default: false},
    isOnSale: {type: Boolean, default:false},
    createdOn: {type: Date, default: new Date()},
    stocks:{type: Number, required:[true, "Stock is required"]},
    order:[{
        userId:{type: String},
        totalQty:{type: Number},
        totalPrice:{type:Number, default:0},
        purchasedOn:{type: Date,default:new Date()}
    }]
})

module.exports=mongoose.model('Products',productSchema)